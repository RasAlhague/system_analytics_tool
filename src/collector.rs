use crate::fetchers::Displayer;
use crate::fetchers::FetchedData;
use crate::fetchers::Fetcher;
use std::collections::HashMap;
use std::error::Error;

pub struct SystemDataCollector {
    displayer: Box<dyn Displayer>,
    fetchers: HashMap<String, Box<dyn Fetcher>>,
}

impl SystemDataCollector {
    pub fn new(displayer: Box<dyn Displayer>) -> SystemDataCollector {
        let fetchers = HashMap::new();

        SystemDataCollector {
            displayer,
            fetchers,
        }
    }

    pub fn run(&self) -> Result<Vec<Box<dyn FetchedData>>, Box<dyn Error>> {
        let mut collected_data = Vec::new();

        self.displayer
            .display_message("Started system data collector!")?;

        for (_, fetcher) in &self.fetchers {
            let data = fetcher.fetch();

            collected_data.push(data);
        }

        self.displayer
            .display_message("Finished collecting system data!")?;

        Ok(Vec::new())
    }

    pub fn add_fetcher(mut self, fetcher: Box<dyn Fetcher>) -> SystemDataCollector {
        let name = fetcher.name();

        if !self.fetchers.contains_key(&name.clone()) {
            self.fetchers.insert(name, fetcher);
        }
        self
    }
}
