use std::error::Error;
use std::path::Path;

pub trait Fetcher {
    fn name(&self) -> String;
    fn fetch(&self) -> Result<Box<dyn FetchedData>, Box<dyn Error>>;
}

pub trait FetchedData {
    fn write(&self, path: &Path) -> std::io::Result<()>;
}

pub trait Displayer {
    fn display_message(&self, str: &str) -> Result<(), Box<dyn Error>>;
    fn display_error(&self, error: &str, message: &str) -> Result<(), Box<dyn Error>>;
}
