use std::collections::HashMap;
use crossterm::{
    event, execute,
    style::{Color, Print, ResetColor, SetBackgroundColor, SetForegroundColor},
    ExecutableCommand, Result, terminal, cursor, QueueableCommand
};
use std::io::{stdout, Write};

pub struct Menu {
    title: String,
    description: Option<String>,
    options: HashMap<char, String>,
}

impl Menu {
    pub fn new(title: &str) -> Menu {
        Menu {
            title: String::from(title),
            description: None,
            options: HashMap::new(),
        }
    }

    pub fn with_description(mut self, value: &str) -> Self {
        self.description = Some(String::from(value));
        self
    }

    pub fn add_option(mut self, key: char, option_text: &str) -> Self {
        self.options.insert(key, String::from(option_text));
        self
    }

    pub fn display(&self) -> Result<()> {
        stdout()
            .queue(cursor::RestorePosition)?
            .queue(terminal::Clear(terminal::ClearType::FromCursorDown))?
            .queue(Print(&self.title))?
            .queue(cursor::MoveToNextLine(1))?
            .queue(Print("---------------------------------"))?
            .queue(cursor::MoveToNextLine(1))?;

        if let Some(desc) = &self.description {
            stdout().queue(Print(desc))?;
        }

        stdout()
            .queue(cursor::MoveToNextLine(1))?
            .queue(Print("---------------------------------"))?
            .queue(cursor::MoveToNextLine(1))?;

        for (key, text) in &self.options {
            stdout()
                .queue(Print(format!(" <{}>: {}", key, text)))?
                .queue(cursor::MoveToNextLine(1))?;
        }

        stdout()
            .queue(cursor::MoveToNextLine(1))?
            .queue(Print("---------------------------------"))?
            .queue(cursor::MoveToNextLine(1))?
            .queue(Print("Please select an option!"))?
            .queue(cursor::MoveToNextLine(2))?
            .queue(Print(">>"))?;

        stdout().flush()?;

        Ok(())
    }
}

