use std::time::SystemTime;
use std::path::Path;
use crate::fetchers::Fetcher;
use crate::fetchers::FetchedData;
use std::error::Error;
use crate::fetchers::Displayer;

pub struct DriverFetcher {
    displayer: Box<dyn Displayer>,
}

impl DriverFetcher {
    pub fn new(displayer: Box<dyn Displayer>) -> DriverFetcher {
        DriverFetcher {
            displayer,
        }
    }
}

impl Fetcher for DriverFetcher {
    fn name(&self) -> String {
        String::from("Driver")
    }

    fn fetch(&self) -> Result<Box<dyn FetchedData>, Box<dyn Error>> {
        unimplemented!();
    }
}

pub struct DriverContent {
    pub drivers: Vec<DriverModel>
}

impl DriverContent {
    pub fn new() -> Self {
        DriverContent {
            drivers: Vec::new()
        }
    }
} 

impl FetchedData for DriverContent {
    fn write(&self, path: &Path) -> std::io::Result<()> {
        unimplemented!();
    }
}

pub struct DriverModel {
    class_guid: String,
    compat_id: String,
    description: String,
    device_class: String,
    device_id: String,
    device_name: String,
    dev_loader: String,
    driver_date: String,
    driver_name: String,
    driver_version: String,
    friendly_name: String,
    hardware_id: String,
    inf_name: String,
    install_date: SystemTime,
    is_signed: bool,
    location: String,
    manufacturer: String,
    name: String,
    pdo: String,
    driver_provider_name: String,
    signer: String,
    started: bool,
    start_mode: String,
    status: String,
    system_creation_class_name: String,
    system_name: String,
}