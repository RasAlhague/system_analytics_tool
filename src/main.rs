mod collector;
mod fetchers;
mod menu;
mod driver_fetcher;
mod event_fetcher;

use crossterm::{
    event, execute,
    style::{Color, Print, ResetColor, SetBackgroundColor, SetForegroundColor},
    ExecutableCommand, Result, terminal, cursor, QueueableCommand
};
use std::io::{stdout, Write};
use crate::menu::Menu;
use crossterm::event::{read, Event, KeyEvent, KeyCode};

fn main() -> Result<()> {
    stdout()
        .execute(terminal::Clear(terminal::ClearType::All))?;

    stdout().execute(Print(format!(
        "{} v.{}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    )))?;

    stdout()
        .execute(cursor::MoveToNextLine(2))?
        .execute(cursor::SavePosition)?;

    show_start_menu()?;

    Ok(())
}

fn show_start_menu() -> Result<()> {
    let menu = Menu::new("Main menu:")
        .with_description("A tool to analyse the system for error finding.")
        .add_option('1', "System data collection")
        .add_option('0', "Exit");

    loop {
        menu.display()?;

        if let Event::Key(event) = read()? {
            match event.code {
                KeyCode::Char('0') => return Ok(()),
                KeyCode::Char('1') => return show_sdc_menu(),
                _ => (),
            };
        }
    }
}

fn show_sdc_menu() -> Result<()> {
    let menu = Menu::new("System data collector:")
        .with_description("Collects data from your system to send it to your supporter.")
        .add_option('1', "Minimal mode")
        .add_option('2', "Full mode")
        .add_option('3', "Advanced mode")
        .add_option('0', "Exit");

    loop {
        menu.display()?;

        if let Event::Key(event) = read()? {
            match event.code {
                KeyCode::Char('1') => println!("Minimal mode"),
                KeyCode::Char('2') => println!("Full mode"),
                KeyCode::Char('3') => println!("Advanced mode"),
                KeyCode::Char('0') => return Ok(()),
                _ => (),
            };
        }
    }
}


